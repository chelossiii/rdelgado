<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
	const INIT_MESSAGE = "////////////////////////////////////////////////\nWelcome to the unit test section.\nYou must complete and fix these unit tests\n////////////////////////////////////////////////";
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
    	$this->withoutMiddleware();
        $this->assertTrue(false, self::INIT_MESSAGE);
    	$response = $this->call('GET', '/api/users');
    	$response->assertJson(["data"=>[]]);
    	$response->assertJsonFragment([
    		"usr_username" => "admin",
    	]);
    	$approvement = $this->call('POST', '/api/users/me/approve');
    	$approvement->assertJson(["status"=>"APPROVED"]);
    }
}
